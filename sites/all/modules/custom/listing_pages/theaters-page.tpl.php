<?php 

$i=1;
	foreach ($city_array as $state) {
		//dpr($state);
		if (array_key_exists('state', $state))
		{
?>
			<div class="state_data_class">
			<h2><strong><?php print $state['state']; ?></strong></h2>
<?php
			foreach ($state['city'] as $city) {
?>
				<div class="city_data_class" id="city_data_id">
					<h2><?php print $city['city']; ?></h2>
					<div class="theaters_data_class">
				<?php foreach ($city['theater'] as $t_nid) { ?>
								<div class="fields title-box">
									<h2><a href="#open-<?php print $i;?>" class="accordion-link"> <?php print $theaters[$t_nid]->title;?></a></h2>
								</div>
								<div class="fields more-info-detail" id="open-<?php print $i?>">
									<div class="region box-2">
									<?php if (isset($theaters[$t_nid]->field_actual_photo['und'][0]['uri']) && !empty($theaters[$t_nid]->field_actual_photo['und'][0]['uri'])) { ?>
										<div class="image-left">
											<img class="left" src="<?php print file_create_url($theaters[$t_nid]->field_actual_photo['und'][0]['uri']);?>" />
										</div>
									<?php } ?>
									<?php if (isset($theaters[$t_nid]->body['und'][0]['value']) && !empty($theaters[$t_nid]->body['und'][0]['value']) ) { ?>
										<div class="info">
											<?php print $theaters[$t_nid]->body['und'][0]['value'];?>
										</div>
									<?php } ?>
										<?php if (array_key_exists($t_nid, $all_theaters)) { ?>
														<div class="fields box">
															<?php	foreach ($all_theaters[$t_nid] as $bid => $sid) { ?>
																	<div class="list-container">
																		<div class="info-name">
																			<p><?php print $show_details[$sid]->title; ?></p>
																		</div>
																		<div class="date-info">
																			<?php print date('d-M-Y', $nodes[$bid]->field_date['und'][0]['value']); ?>
																		</div>
																		<div class="time-info">
																			<div class="tool-tip">
																				<span class="tool-tip-info">
																					<a href="<?php echo url('book-now',array('query'=>array('bid'=>$bid)))?>"><?php echo $nodes[$bid]->field_time['und'][0]['value']; ?></a>
																				</span>
																			</div>
																		</div>
																	</div>
<?php													}
?>
														</div>
<?php											} else {
?>
													<div class="fields box">
														<p><?php print "No Shows Available."; ?></p>
													</div>
<?php											}
?>
										
									</div>
								</div>
<?php	$i++; 
							} 
?> </div></div><?php
			}
			?></div><?php	
		}

	}
?>
