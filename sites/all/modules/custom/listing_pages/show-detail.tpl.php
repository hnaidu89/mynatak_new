
<div class="fields">
	<h1><?php print $node->title;?></h1>
</div>
<div class="fields">
	<div class="region box">
			<div class="image-left"><img class="left" src="<?php print file_create_url($node->field_show_image['und'][0]['uri']);?>" /></div>
			<div class="info">
				<?php print $node->body['und'][0]['value'];?>
			</div>
	</div>	
</div>
<h1 class="region size2of3"><?php print 'List of Theaters for ' ?><span class="list-name"><b><?php print ucfirst($node->title) ?></b></span> <?php print 'Show'; ?> </h1>
<div class="fields">
	<div class="region box">
		<?php foreach ($all_shows[$node->nid] as $bid => $tid) { ?>
								<div class="list-container">
									<div class="info-name">
										<p><?php print $theater_details[$tid]->title; ?> : <span class="palce"><?php print _get_state_city($theater_details[$tid]->field_city['und'][0]['value']); ?></span></p>
									</div>
									<div class="date-info">
										<?php print date('d-M-Y', $booking[$bid]->field_date['und'][0]['value']); ?>
									</div>
									<div class="time-info">
										<div class="tool-tip">
											<span class="tool-tip-info">
												
												<a href="<?php echo url('book-now',array('query'=>array('bid'=>$bid)))?>"><?php echo $booking[$bid]->field_time['und'][0]['value']; ?></a>
											</span>
										</div>
									</div>
								</div>
		<?php } ?>
	</div>
</div>