<?php 

if (!empty($data['show'])) {?>
<div class="fields">
	<h1><?php print $data['show']->title;?></h1>
</div>
<div class="fields">
	<div class="region box">
		<div class="image-left"><img class="left" src="<?php print file_create_url($data['show']->field_show_image['und'][0]['uri']);?>" /></div>
			<div class="info">
				<?php print $data['show']->body['und'][0]['value'];?>
			</div>
		</div>
</div>
<?php } ?>
<?php if (!empty($data['theater']) && empty($data['show'])) { ?>
<div class="fields">
	<h1><?php print $data['theater']->title;?></h1>
</div>
<div class="fields">
	<div class="region box">
		<?php if (isset($data['theater']->field_actual_photo['und'][0]['uri'])): ?>
			<div class="image-left"><img class="left" src="<?php print file_create_url($data['theater']->field_actual_photo['und'][0]['uri']);?>" /></div>
		<?php endif; ?>
			<div class="info">
				<ul>
					<li class="odd">State: </li>
					<li class="odd"><?php print _get_state_city($data['theater']->field_state['und'][0]['value']);?></li>
					<li class="odd">City: </li>
					<li class="odd"><?php print _get_state_city($data['theater']->field_city['und'][0]['value']);?></li>
					
							</ul>
				<?php print $node->body['und'][0]['value'];?>
			</div>
	</div>
</div>
<?php } ?>
<div class="fields">
	<?php print $details['text']; ?>
</div>
<div class="fields">
	<div class="region box">
	<?php if ($details['flag'] == 'both') { ?>
		<div class="list-container">
			<div class="info-name">
				<p><?php print $data['theater']->title; ?> : <span class="palce"><?php print $details['city'][$data['theater']->field_city['und'][0]['value']]; ?></span></p>
			</div>
			<?php foreach ($details['booking_nodes'] as $node) {
				//dpr($node);
			?>
				<div class="date-info">
				<?php print date('d-M-Y', $node->field_date['und'][0]['value']); ?>
			</div>
			<div class="time-info">
				<div class="tool-tip">
					<span class="tool-tip-info">
						<a href="<?php echo url('book-now',array('query'=>array('bid'=>$node->nid)))?>"><?php echo $node->field_time['und'][0]['value'];?></a>
					</span>
				</div>
			</div>
			
			<?php } ?>
		</div>
	<?php } elseif($details['flag'] == 'show') { 
						foreach ($details['booking_nodes'] as $node) {
?>
							<div class="list-container">
							<div class="info-name">
								<p><?php 
								print $details['theater_details'][$node->field_theatre_input['und'][0]['nid']]->title ; ?> : <span class="palce"><?php print $details['city'][$details['theater_details'][$node->field_theatre_input['und'][0]['nid']]->field_city['und'][0]['value']]; ?></span></p>
							</div>
							
								<div class="date-info">
								<?php print date('d-M-Y', $node->field_date['und'][0]['value']); ?>
							</div>
							<div class="time-info">
								<div class="tool-tip">
									<span class="tool-tip-info">
										<a href="<?php echo url('book-now',array('query'=>array('bid'=>$node->nid)))?>"><?php echo $node->field_time['und'][0]['value'];?></a>
									</span>
								</div>
							</div>
							</div>
<?php 			} 
				} elseif($details['flag'] == 'theater') { 
					//dpr($details);exit;
						foreach ($details['booking_nodes'] as $node) {
?>
							<div class="list-container">
								<div class="info-name">
									<p><?php 
									print $details['show_details'][$node->field_show_input['und'][0]['nid']]->title ; ?> </p>
								</div>
								
									<div class="date-info">
									<?php print date('d-M-Y', $node->field_date['und'][0]['value']); ?>
								</div>
								<div class="time-info">
									<div class="tool-tip">
										<span class="tool-tip-info">
											<a href="<?php echo url('book-now',array('query'=>array('bid'=>$node->nid)))?>"><?php echo $node->field_time['und'][0]['value'];?></a>
										</span>
									</div>
								</div>
							</div>
<?php 			} 
				} 
?>
</div>
</div>