<?php 

$i=1;?>

<?php foreach ($all_shows as $key => $value) { ?>
				<div class="fields title-box">
					<h2><a href="#open-<?php print $i;?>" class="accordion-link"> <?php print $show_details[$key]->title;?></a></h2>
					<!---<div class="more-link">
						<a href="<?php print url('search_result',array('query' => array('show' => $key)))?>">Book Now &raquo;</a>
					</div>-->
				</div>
				<div class="fields more-info-detail" id="open-<?php print $i?>">
					<div class="region box-2">
						<div class="image-left">
							<img class="left" src="<?php print file_create_url($show_details[$key]->field_show_image['und'][0]['uri']);?>" />
						</div>
						<div class="info">
							<?php print $show_details[$key]->body['und'][0]['value'];?>
						</div>
						<div class="fields box">
							<?php foreach ($value as $bid => $tid) { ?>
								<div class="list-container">
									<div class="info-name">
										<p><?php print $theater_details[$tid]->title; ?> : <span class="palce"><?php print _get_state_city($theater_details[$tid]->field_city['und'][0]['value']); ?></span></p>
									</div>
									<div class="date-info">
										<?php print date('d-M-Y', $nodes[$bid]->field_date['und'][0]['value']); ?>
									</div>
									<div class="time-info">
										<div class="tool-tip">
											<span class="tool-tip-info">
												<a href="<?php echo url('book-now',array('query'=>array('bid'=>$bid)))?>"><?php echo $nodes[$bid]->field_time['und'][0]['value']; ?></a>
											</span>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
<?php	$i++; }
?>
