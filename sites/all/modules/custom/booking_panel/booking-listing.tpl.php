<?php
	global $theme,$base_url;
	static $j = 0;
	$t_path = $base_url.'/'.drupal_get_path('theme',$theme);
	if(is_object($data)): ?>
		<?php 
			$movie_title = _get_state_city($data->field_show_input['und'][0]['nid']);
			$theater_details = node_load($data->field_theatre_input['und'][0]['nid']);
			$row_type_array = array(174 => 'A');
			$start_row_name = $row_type_array[$data->field_row_identity['und'][0]['value']];
		?>
<div id="tblClassNew" class="bkcl callor">
	<div class="bkclbod callwhite">
		<div class="padbuy">
			<div class="hdname">
				<div style="width:auto; padding-right:20px;" class="etname" id="SLEName_New"><?php echo $movie_title;?></div>
				<span id="SLNew_CenMsg" class="adultmov" style="width: auto; padding-top: 12px; display: none;">Adult Movie</span>
				<div class="etname" style="width:940px;">
					<span id="ShowTym_New" style="font-size:12px;"><?php echo date('d-M',$data->field_date['und'][0]['value']);?> | <?php echo $data->field_time['und'][0]['value'];?></span><span style="padding:0 5px;">|</span><span style="font-size:12px;" id="VenAdd_New"><?php echo $theater_details->title;?></span>&nbsp;&nbsp; <a style="font-size: 12px; font-weight:normal;" href="<?php url('theaters');?>">( <span style="text-decoration:underline;">Choose another Show</span> )</a>
				</div>
			</div>
			<div class="callact" style="padding-top:0px;">
				<div class="celaqt" style="width:auto;">
					<span style="float:left; padding:5px 13px;">Quantity :</span>
					<select id="SLcmbQty" class="qtselect" onchange="fnSQty();"><option value="">Qty</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>
				</div>
				<div style="float:right">
					<div style="float:left; font-size:14px; font-weight:bold; padding:5px; margin-right: 20px;">Total: <span id="STotal" style="font-size: 14px; font-weight: normal;">Rs.0</span></div>
					<div style="float:left;"><a class="button yellow" onclick="fnPreInit();" href="javascript:;">Proceed to checkout</a></div>
				</div>
			</div>
			<div style="border-top:0px; padding-top:0px; left:0px;" class="seatlayout">
				<div id="ShwMorSeats" style="display: none;">
					<p style="opacity: 1;">Scroll <br><span style="padding:0 8px;">for</span><br> more</p>
					<div class="small-arrow-down" style="opacity: 1;"></div>
				</div>
				<div style="overflow:auto;" class="actseata">
					<div id="layout" style="width: 770px; margin: 0px auto;">
						<table cellspacing="0" cellpadding="0" style="width:100%px;" class="setmain">
							<tbody>
							<?php foreach ($data->field_ticket_pricing['und'] as $class):?>
								<?php list($class_name,$price,$from,$to) = _get_calss($data->field_ticket_pricing['und'][0]['value']);?>
									<tr>
										<td class="seatR">&nbsp;</td>
										<td class="PriceB1" style="background:#f0f6dc;">
											<div class="seatP" style="background:#f0f6dc;">
												<div class="fleft"><?php echo $class_name;?>(<?php echo $price;?>)</div>
											</div>
										</td>
									</tr>
									<?php for($i = $from; $i <= $to; $i++):?>
									<tr>
										<td><div class="seatR Setrow1"><?php echo $i;?></div></td>
										<td class="SRow1" style="background:#f0f6dc">
												<?php $number_of_seats = _get_number_of_seats($theater_details->field_seats_per_row['und'][$j]['value']);
														list($start,$end) = _get_seating_arrag($data->field_seating['und'][$j]['value']);
														for($k=1; $k<=$number_of_seats; $k++):
																 $image_name = '1_2.gif';
																 $on_click  = $href = '';
																 $class = "non-avail";
																 if($k >= $start && $k <= $end){
																	$image_name = '1_1.gif';
																	$href="href=javascript:void(0)";
																	$class = "avail";
																	}
																?>
																<div class="seatI" style="background:#f0f6dc"><a class="<?php echo $class?>" <?php echo $href?> <?php echo $on_click?>><img id="<?php echo $i.$k?>"src="<?php echo$t_path?>/images/<?php echo $image_name;?>"></a>
																</div>
														<?php endfor; $j++;?>
										</td>
									</tr>
										<?php endfor;?>	
								<tr><td colspan="2">&nbsp;</td></tr>										
							<?php endforeach;?>
							</tbody></table></div>
				</div>
			</div>
			<div class="callact">
			<div class="celaqt" style="width:673px;">
				<div class="screenarro "><span class="ic-ardn" style="font-size:16px; color:#fff;"></span></div>
				<div class="scrtw">SCREEN THIS WAY </div>
			</div>
			<div style="flot:right; "><a class="button yellow" onclick="fnPreInit();" href="javascript:;">Proceed to checkout</a></div>
		</div>	
		</div>
	</div>
</div>
<!-- NEW BOOKING FLOW -->


<!-- DIV for multiple category booking - BEGIN -->
<style type="text/css">
	.dc		{ padding: 10px 5px !important; margin-right: 15px; width: 230px; } /* category */
	.dq		{ float: left; } /* quantity */
	.dp		{ float: left; } /* price */
	.dt		{ float: left; width: 150px; padding: 10px 0px; } /* total */
	.dtot	{ width: 200px; padding: 5px 0px 0px 10px; font-size: 20pt; font-weight: bold; } /* grand total */
	.ticktGrnbtn .fl_100{ padding-top:10px;text-align:center;}
	.ticktGrnbtn ul{ float:left; margin-left:15px; padding-left:10px; list-style:decimal;}
</style>
<div id="tblClassMultiCat" class="bkcl callor" style="display:none;">
	<div class="bkclbod callwhite">
		<a href="javascript:;" onclick="fnCnCl();"><div class="cross fright"></div></a>
		<div class="padbuy">
			<div class="hdname">
				<div id="EName_MC" class="etname" style="width:auto;">Goliyon Ki Raasleela Ram-Leela (U/A)</div>
				<div id="EDtl_MC" class="ciname">
					<span>Tomorrow, 17 Nov, 08:50 AM</span>
					<span class="cinema">CineMAX: Growel, Kandivali (E)</span>
				</div>
				<span id="CenMsg_MC" class="adultmov" style="display: none;">Adult Movie</span>
			</div>
			<div class="fl_100">				
				<div style="float: left; width: 488px;">					
					<div id="category_MC"></div>	
					<div style="float: left; width: 200px;">
						<a id="btnProceed_MC" href="javascript:;" onclick="fnInitMultiCatBook();" class="button yellow">Proceed</a>
					</div>
					<div id="divTotal_MC" class="fleft dtot">Rs. 0.00</div>
					<div class="fl_100">						
						<div class="clear">&nbsp;</div>
						<div id="errMC" class="error mypagerr" style="display: none;"></div>
						<div class="clear">&nbsp;</div>
					</div>
				</div>					
				<div style="float: left; width: 350px;">
					<b style="font-size:13px;">Important notes:</b>
					<ul style="padding-top:5px; margin-left: 15px; font-size:11px; list-style:decimal; line-height:16px;">
						<li><b>Child:</b> 3 - 12 years of age</li>
						<li><b>Adult:</b> 13 years and above</li>
						<li><b>Infants:</b> Under the age of 3 and not required to purchase a Park ticket</li>
						<li><b>Disabled individuals:</b> No differentiation from any other guests coming to the park in terms of price policy. A spot policy to be put in place to make such guests more comfortable. The Disabled individual will pay for the ticket price as per their eligibility of Adult or Child</li>
						<li><b>Imagica Express mechanism:</b> a. Single entry to 11 finalized attractions (1. Alibaba aur Chaalis Chorr; 2. D2 - Dare Drop; 3. Deep Space; 4. Gold Rush Express; 5. Mr. India - The Ride; 6. Curse of Salimgarh; 7. Scream Machine; 8. Rajasaurus River Adventure; 9. I for India; 10. Wrath of The Gods; 11. Cinema 360 Prince of The Dark Waters)</li>
						<li><b>Family(*2 Adult 2 Child):</b> Equivalent to 4 persons with 2 persons equivalent to 'Child' and 2 persons equivalent to 'Adult'</li>
						<li><b>2 Day pass:</b> Individual tickets will be given each day at the ticketing counter.</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</div>
<!-- DIV for multiple category booking - END -->
<!--SECOND-->
<div id="tblSeatLayout" class="bkcl callor" style="display:none;">
	<div class="bkclbod callwhite">
		<a href="javascript:;" onclick="fnCanTr();"><div class="cross fright"></div></a>
		<div class="padbuy">
			<div class="hdname">
				<div id="SLEName" class="etname">Goliyon Ki Raasleela Ram-Leela (U/A)</div>
				<div id="SLEDtl" class="ciname">
					<span>Tomorrow, 17 Nov, 08:50 AM</span>
					<span class="cinema">CineMAX: Growel, Kandivali (E)</span>
				</div>
			</div>
			<!--div class="buyselbox">
			<select class="wt230"><option>Ticket Type</option></select>
			<select class="wt80"><option>Qty</option></select>
			<div class="ntrgt"><strong>Note :</strong> The colors shown on the category drop down represent each category in the seat layout below.</div>	
			</div-->
			<div class="seatlayout" style="border-top:0px; padding-top:0px;">
				<div class="actseat" style="overflow:auto;">
					<div id="tblSeatInfo"></div>
					<!--<div class="fleft" style="margin:0 auto;"><img src="http://cnt.in.bookmyshow.com/bmsin/buytickets/stw.jpg" height="61" width="540px"/></div>-->
				</div>
				<div class="seatleg">
					<div class="leghead">Key to Seat Layout :</div>
					<div class="legendseat">
						<div class="chair"><img src="<?php echo$t_path?>/images/W_chair.gif" alt=""></div>
						<div class="seatxt">Available</div>
					</div>
					<div class="legendseat">
						<div class="chair"><img src="<?php echo$t_path?>/images/R_chair.gif" alt=""></div>
						<div class="seatxt">Booked</div>
					</div>
					<div class="legendseat">
						<div class="chair"><img src="<?php echo$t_path?>/images/G_chair.gif" alt=""></div>
						<div class="seatxt">Your Selection</div>
					</div>
					<div class="legendseat">
						<div class="chair"><img src="<?php echo$t_path?>/images/Gy_chair.gif" alt=""></div>
						<div class="seatxt">Unavailable</div>
					</div>
					<div class="friendseat">
						<div class="frstab" id="fbPicsHead" style="display: none;">
							<div class="chair"><img src="<?php echo$t_path?>/images/fb_chair.gif" alt=""></div>
							<div class="seatxt">Friends</div>
						</div>
						<div class="frcont" id="fbPics" style="display: none; width: 160px;">
							<span>Know where your friends are seated?</span>
							<div class="fbfriends">
								<a class="back fbbackmar"></a>
								<img src="<?php echo$t_path?>/images/attending.png" width="50" height="50" alt="">
								<img src="<?php echo$t_path?>/images/attending.png" width="50" height="50" alt="">
								<img src="<?php echo$t_path?>/images/attending.png" width="50" height="50" alt="">
								<a class="forward fbformar"></a>
							</div>
						</div>
					</div>
					<a href="javascript:;" onclick="fnsubSetSeats();" class="button yellow btnspacest">Proceed</a>
					<div id="seatErr" class="error mypagerr" style="display: none;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--SECOND-->
<!--THIRD-->
<div id="oddsm" class="bkcl callor" style="display:none;">
	<div class="bkclbod callwhite">
		<a href="javascript:;" onclick="fnCanTr();">
		  <div class="cross fright"></div>
		</a>
		<div class="padbuy">
			<div class="hdname">
				<div id="OSEName" class="etname">Spiderman 3D (U/A)</div>
				<div id="OSEDtl" class="ciname">
					<span>23rd July, 2012, 07:00 PM	</span>
					<span class="cinema">Inox Milan Mall (Santacruz)</span>
				</div>
			</div>
			
			<!-- LHS OF ORDER SUMMERY STARTS -->
			<div class="odsumlft">
				
				<!-- OFFERS SECTION STARTS -->
				<div id="ErnPt" class="callrgtbl" style="border-bottom:0px; margin-bottom:0;">
					<!--<img src="http://cnt.in.bookmyshow.com/in/buytickets/visablockbuster.jpg" width="568" alt="" />-->
					<div style="background: url(http://cnt.in.bookmyshow.com/bmsin/callouts/Donation.jpg) no-repeat; width:580px;height: 139px;border: 1px #ccc solid;border-radius: 5px;">
						<div style="width: 245px; float:right;padding-top: 5px;">
							<div class="fl_100" style="padding-bottom:3px;">
								<p style="color:#c82528; padding-top:5px; float:left; font-size:14px; font-weight:bold;">A Day At The Movie..</p><br>
								<p style="padding-top:5px; line-height:16px; float: left;">might be your favourite timepass but for some it is a far-fetched dream. We along with The Akanksha Foundation strive to make their dream a reality. <a target="_blank" href="http://in.bookmyshow.com/donation/">Read More</a></p>
							</div>
							<div class="fl_100" style="border-top:1px solid #ccc; padding-top:3px;">
								<p style="width: 50px; float: left;"><span style="font-size: 12px;">Re. 1</span><input id="FLDonation" type="checkbox" value="Rs" name="rsCheck" style="margin-top:5px;"></p>
								<p>will be added to your transaction as a donation</p>
							</div>
						</div>
					</div>
          <div id="divAdlabs" style="float:left;width:560px;"></div>
				  <!--div class="offerscal">
					
					<div class="offersmain">
					  <p>Visa Blockbuster Weekend offer 1</p>
					  <div class="tico">
						<div style="margin-right:10px;" class="fleft">
						  <img width="70" src="http://cnt.in.bookmyshow.com/bmsin/offers/VISA.png" alt=""/>
						</div>
						<div class="fleft wt80">Buy 1 Get 1 free</div>
					  </div>
					</div>
					<div class="tickstb">&nbsp;</div>
					<div class="offersmain">
					  <p>ICICI Bank Experience Ent offer</p>
					  <div class="tico">
						<div style="margin-right:10px;" class="fleft">
						  <img width="70" src="http://cnt.in.bookmyshow.com/bmsin/offers/ICICIbank.png" alt=""/>
						</div>
						<div class="fleft wt80">Rs 100 off</div>
					  </div>
					</div>
					<div class="tickstb">&nbsp;</div>
					<div class="offersmain">
					  <p>Citibank Second is Free Debit Offer</p>
					  <div class="tico">
						<div style="margin-right:10px;" class="fleft">
						  <img width="70" src="http://cnt.in.bookmyshow.com/bmsin/offers/Citibank.png" alt=""/>
						</div>
						<div class="fleft wt80">Buy 1 Get 1 Free</div>
					  </div>
					</div>
				  </div-->
				</div>
				<!-- OFFERS SECTION ENDS -->
				
				<!-- ETICKET STARTS -->
				<div id="OSETickets" class="callrgtbl" style="display:none;">
					<h6>Ticket Type :</h6>
					<div class="fl_100">
						<div class="ticket">
							<div class="icotickssel">
								<span class="eticket"></span>
						</div>
						<div class="icotext">
							<div class="selection">
								<input id="rEticketHO" name="ETicket" type="radio"> E-Ticket
							</div>
							<div class="fl_100">
                <strong>
                  Print the e-ticket and and bring<br> it with you.</strong>
                <br>
                  (No Need to wait in line at the <br> box office)               
              </div>
							</div>
						</div>
						<div class="ticket">
							<div class="icoticks">
								<span class="boxoffice"></span>
							</div>
							<div class="icotext">
								<div class="selection">
									<input id="rEticketBO" name="ETicket" type="radio"> Box Office Pickup
								</div>
								<div class="fl_100">
									Select this option to pick your<br> tickets from Box Office
								</div>
							</div>
						</div>
						<!-- ERROR DIV FOR ETICKETS -->
						<div id="ETickErr" class="error mypagerr" style="display: none;"></div>
					</div>
				</div>
				<!-- ETICKET ENDS -->
			
				<!-- FOOD COMBO STARTS -->
				<div id="OSFodCmb" class="callrgtbl" style="display:none;">
					<h6>ADD-ONS</h6>
					<div id="OSFCInBox" class="foodcombo" style="width:566px;">
						<a id="FCPrev" href="javascript:;" class="back fdbackmar" onclick="fnFrdDC(&#39;PREV&#39;);"></a>
						<div id="outerBox" style="padding:0; margin:0; overflow:hidden; width:534px; float:left;">
							<div style="width:9999px;"></div>
						</div>
						<a id="FCNxt" href="javascript:;" class="forward fdformar" style="float:left;" onclick="fnFrdDC(&#39;NEXT&#39;);"></a>
					</div>
					<div id="ColMod" class="fl_100 bold" style="display:none;">
						When you would like to collect your combo 
						<select id="selDelMode" class="fcselect">
							<option value="">-- Collect at Counter --</option>
							<option value="Before Start of Movie">Counter : Before Start of Movie</option>
							<option value="During Interval">Counter: During Interval</option>
						</select>
					</div>
					<!-- FOR DT CINEMAS -->
					<div id="DelModeDT"></div>
					<!-- ERROR DIV FOR FOOD COMBO -->
					<div id="FCErr" class="error mypagerr" style="display: none;"></div>
				</div>
				<!-- FOOD COMBO ENDS -->
			
								
				<!-- NOTES STARTS -->
				<div class="callrgtbl">
					<span class="bold fl_100">Note</span>
					<div class="fl_100">
						<div class="dqnote" id="dGopalanChildNode" style="display: none; background-color: rgb(255, 255, 255); width: 100%; text-align: justify; background-position: initial initial; background-repeat: initial initial;">
							Tickets mandatory for guests above 3 years of age.
						</div>
						<div id="dFNote" class="fleft" style="background-color: rgb(255, 255, 255); width: 100%; display: none; text-align: justify; background-position: initial initial; background-repeat: initial initial;">
							<strong>Please Note: For 3D glasses: </strong>
							<br>
							Bangalore: Rs.30 (non-refundable usage charge) collected online as 'additional charges' &amp; Rs.50 refundable deposit payable at the cinema<br>
							Bhopal: Rs.20 (non-refundable usage charge) collected online as 'additional charges' &amp; Rs.100 refundable deposit payable at the cinema<br>
							Other centers: Rs.20 OR 30 (non-refundable usage charge) &amp; Rs.50 OR 100 (refundable deposit) payable at the cinema.<br>
						</div>
						<div class="dqnote" id="dABICNote" style="display: none; background:#FFFFFF; width:100%; text-align:justify;">
							<strong>Please Note: </strong>Ticket Categories with Massage/Recliner feature and/or 3D movies may incur an extra charge. Please check the exact amount on the payment page before proceeding.
						</div>
						<div class="dqnote" id="fdNote" style="display:none; background:#FFFFFF; width:100%; text-align:justify;">
								Bring along your confimation printout to collect the food combo.
						</div>
						<div class="dqnote" id="dFNote" style="display:none; background:#FFFFFF; width:100%; text-align:justify;"></div>
						<div class="dqnote" id="dABICNote" style="display: none; background:#FFFFFF; width:100%; text-align:justify;">
							Ticket Categories with Massage/Recliner feature and/or 3D movies may incur an extra charge. Please check the exact amount on the payment page before proceeding
						</div>
						<div class="dqnote" id="dPrintSMSCompulsoryNote" style="display: none; background-color: rgb(255, 255, 255); width: 100%; text-align: justify; background-position: initial initial; background-repeat: initial initial;">
							Customers have to carry the printout and SMS of the booking confirmation in order to collect the tickets at the Booking Counter without which the cinema will not issue tickets.
						</div>
						<div class="dqnote" id="dPrintOutComp" style="display: none; background-color: rgb(255, 255, 255); width: 100%; text-align: justify; background-position: initial initial; background-repeat: initial initial;">
							Printout is Mandatory in order to collect the tickets at the Booking Counter.
						</div>
						<div id="spNote" class="dqnote" style="background:#FFFFFF; width:100%; text-align:justify;">
						  An Internet handling fee (per ticket) will be levied. Check the same before completing the transaction.
						</div>
						<div class="dqnote" id="d3DNoteOS" style="background-color: rgb(255, 255, 255); width: 100%; text-align: justify; display: none; background-position: initial initial; background-repeat: initial initial;">For 3D Glasses a refundable/non-refundable amount may have to be paid per ticket at the cinema. The exact amount will be intimated at the cinema.<br></div>
					</div>
				</div>
				<!-- NOTES ENDS -->
				
			</div>
			<!-- LHS OF ODER SUMMERY ENDS -->
		  
			<!-- RHS OF ORDER SUMMARY STARTS-->
			<div class="odsumrgt">
				<div class="odcont">
					<h2 class="odhead">Order Summary</h2>
					<div id="dOrderSummary" class="odsum">
						<div class="rptr">
							<div id="OSTikAmt" class="hdr">
								<span class="wt90 bold">Ticket(s):</span>
								<span class="wt130">Rs. 510.00</span>
							</div>
						</div>
						<div id="dTickets" class="rptr">
							<div id="OSSTInfo" class="hdr">
								<span class="wt90 bold">Seat Info:</span>
								<span class="wt130">Gold : B6, B7, B8</span>
							</div>
						</div>
						<div class="rptr">
							<div id="OSBKFee" class="hdr">
								<span class="wt90 bold">+ Internet handling fees:</span>
								<span class="wt130">Rs. 50.00</span>
							</div>
							<div id="dOtherCharges" class="hdr" style="display: none;">
								
							</div>
						</div>
						<div id="OSFC" class="rptr" style="display:none;">
							<div class="hdr">
								<span class="wt90 bold">+ ADD-ONS :</span>
								<span class="wt75">Rs. 450.00</span>
								<!--a href="#" class="showmore">show more</a-->
							</div>
						</div>
						<div id="FCDtl" class="rptr" style="display:none;">
							<span class="fleft" style="width:190px;">Jumbo Combo - Spice Gold</span>
							<span class="fright bold" style="padding-right:13px;">Qty 3</span>
						</div>
						<div id="OSTotal" class="Gtotal" style="background: #FFF9D9;">
							<span>Rs. 1120.00</span>
						</div>

						<!--<div id="OSDonation" class="donation" style="display: none;">
							<div class="donatecall"></div>
							<div class="donrs">
								<span class="bold">A Day At The Movies</span> might be our favorite timepass but for some it is a far-fetched dream...<br>
								<a href="/donation" target="_blank">read more</a>
							</div>
							<div class="bookasmile">
								<span class="dntext">DONATE</span>
								<span class="retext">
									Re.<span class="rstext">1</span>
								</span>
							</div>
							<div class="fl_100">
								<input id="FLDonation" type="checkbox"/> I agree to Donate Re. 1
							</div>
						</div>-->
					</div>
				</div>
				<a href="javascript:;" onclick="fnPrePay();" class="button yellow btnspace" style="margin-left: 55px;">Proceed to Payment</a>
			</div>
			<!-- RHS OF ORDER SUMMARY ENDS-->
		</div>
	</div>
</div>

<!-- ticketgreen CALLOUT -->
<div id="TGExCall" class="bkcl callor" style="display:none;">
	<div class="bkclbod callwhite">
		<a href="javascript:;" id="TGExCl"><div class="cross fright"></div></a>
		<div class="padbuy ticktGrnbtn">
			<h3>Terms &amp; Conditions:</h3>
			<strong>Please read this important Terms and Conditions for the Internet Booking</strong>
			<ul>
				<li style="font-weight:bold;font-size:13px;">Tickets have to be collected 2 Hrs before the Show Time, else the seats would be allotted to other customers automatically. </li>
				<li>Charges collected are towards the reservation charge only, it does not include the ticket cost.</li>
				<li>Ticket cost has to be paid at the counter after producing the booking reservation number.</li>
				<li>BookMyShow does not have any information on the ticket cost.</li>
				<li>Convenience charge will not be refunded if the tickets are not collected on-time at the counter.</li>
			</ul>
			<div class="fl_100">
				<a href="javascript:;" id="TGExPr" class="button yellow">Accept</a>
				<a href="javascript:;" id="TGExCl1" class="button blackbtn">Cancel</a>
			</div>
		</div>
	</div>
</div>		</div>
		
		<script type="text/javascript">
			glBT.nowDate	=	"20131116";
			glBT.nowTime	=	"0145";
			glBT.iv			=	243;
		</script>
		<script type="text/javascript">
			fnPgLd(false);
			var objCk		=	new clsCookie();
			var showReg		=	objCk.get({ name: "doneOnce", key: "RGNMSG", defVal: "" });
			var showSI		=	objCk.get({ name: "doneOnce", key: "RGNMSG", defVal: "" });
			var ckRgn		=	fnGRgn();
			var ckLd		=	objCk.get({ name: "ld", defVal: "" });
			var lastReg		=	objCk.get({ name: "userCine", defVal: "", key: "lastRC" });
			var srid		=	fnGQS("srid", "");
			var srid		=	fnGQS("cid", "");
			
			//	FOR HIGHLIGHTING REGIONS
			function fnBg(blnFlag) {
				try {
					var objDiv;
					if (jq("#dRegBck").length == 0) {
						objDiv = jq("<div />").attr("id", "dRegBck");
						objDiv.css({ position: "absolute", zIndex: "99998" });
						objDiv.css({ top: "0px", left: "0px", width: "100%", height: jq(document).height() });
						jq("body").append(objDiv);
					}
					else {
						objDiv = jq("#dRegBck");
						objDiv.css({ width: "100%", height: jq(document).height() });
					}
					
					if (blnFlag) {
						jq("#dRegBck").fadeIn("fast");
						jq("#MSG , #dSRList").css({"z-index":"99999","position":"relative"});
						jq("#MSG").fadeIn("fast");
					}
					else {
						var objCk		=	new clsCookie();
						objCk.set({ name: "doneOnce", key: "RGNMSG", value: "Y", sess: true });
						jq("#dRegBck").hide();
						jq("#MSG , #dSRList").css({"z-index":"0","position":"relative"});
						jq("#MSG").fadeOut("fast");
					}
				} catch(e) {
					fnErr({ fnName: "fnBg", fnParams: arguments, err: e });
				}
			}
			
			if( ckRgn == "" || ckRgn == "ALL") {
				if(global.RgnCallBack == undefined) {
					global.RgnCallBack = [];
				}
				if(ckLd == "" && showSI != "Y") {
					global.RgnCallBack.push(function() {
						ckRgn		=	fnGRgn();
						if(ckRgn == "MUMBAI" || ckRgn == "NCR") {
							if((srid	== "MUMBAI" || srid == "NCR") && cid == "") {
								fnBg(true);
							}
						}
						else {
							SignIn.fnSignIn(true);
						}
					});
				}
			}
			else if(ckLd == "" && showSI != "Y") {
				if(ckRgn == "MUMBAI" || ckRgn == "NCR") {
					if((srid	== "MUMBAI" || srid == "NCR") && cid == "") {
						fnBg(true);
					}
				}
				else {
					global.RgnCallBack.push(function() {
						SignIn.fnSignIn(true);
					});
				}
			}
			
			// FOR SHOWING FCONNECT
			/*if( ckRgn == "" || ckRgn == "ALL") {
				if(global.RgnCallBack == undefined) {
					global.RgnCallBack = [];
				}
				if(ckLd == "" && showOnce != "Y") {
					global.RgnCallBack.push(function() {
						SignIn.fnSignIn(true);
					});
				}
			}
			else if(ckLd == "" && showOnce != "Y") {
				SignIn.fnSignIn(true);
			}*/
		</script>
		<script type="text/javascript">
			setTimeout(function(){var a=document.createElement("script");
			var b=document.getElementsByTagName("script")[0];
			a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0012/8616.js?"+Math.floor(new Date().getTime()/3600000);
			a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
		</script>
	
<div id="dSignIn" style="display: none; left: 0px; position: absolute; top: 0px; width: 400px; z-index: 2147483647;"><div class="mainr callor"><div class="mainr callwhite"><div class="spacemain" id="mSignInDiv"><div class="fleft" id="fButton"><h3 style="margin:10px 0px 5px 75px;">Use Facebook to Sign in</h3><div id="fbLogin" class="fbkbutton" onclick="window.open(&#39;/fb/login/&#39;,&#39;Login&#39;,&#39;height=500,width=700,scrollbars=yes&#39;);return false;"><div class="ic-fb scfbk"></div><div class="fbktxt">Log in with Facebook</div></div><div class="orlink fleft"></div><div class="circle fleft">OR</div></div><div class="signtab"><div class="fleft wt100"><input type="radio" name="rdSign" class="radio" id="signIn" onclick="SignIn.fnSignIn(true);" checked="checked"><span class="fleft">Sign in</span></div><div class="fleft wt165"><input type="radio" name="rdSign" class="radio" id="register" onclick="SignIn.fnReg();"><span class="fleft">Create an Account</span></div></div><div class="signin fleft"><div class="ic-msg msg"></div><input type="text" id="iUserName" placeholder="Email"></div><div class="signin fleft" id="dSignPwd"><div class="ic-lock msg"></div><input type="password" id="iPwd" placeholder="Password"></div><div class="signin fleft" id="dRegPwd" style="display: none;"><div class="ic-lock msg"></div><input type="password" id="iPwdCon" placeholder="Confirm Password"></div><div style="clear:both;"></div><div id="dSignBtn"><a href="javascript:;" style="font-size:12px;" class="fleft mt5" id="iFPwd" onclick="SignIn.fnFPwd();">Forgot Password?</a><a href="javascript:;" onclick="SignIn.fnValLogIn();" id="iSignIn" class="button yellow btnsign">Sign In</a><a href="javascript:;" onclick="SignIn.fnClose({ callBack: false });" style="margin:5px 10px 0px 0px;" class="btnsign">Close</a></div><div class="fright" style="display:none;" id="ldng"><img alt="Loading.." id="Loading" src="<?php echo$t_path?>/images/loader.gif"></div><div class="fleft" style="display:none;" id="Fldng"><img alt="Loading.." id="Loading" src="<?php echo$t_path?>/images/loader.gif"></div><div class="fright" id="dRegBtn" style="display: none;"><a href="javascript:;" onclick="SignIn.fnValReg();" id="iReg" class="button yellow btnsign">Register</a><a href="javascript:;" onclick="SignIn.fnClose({ callBack: false });" style="margin:5px 10px 0px 0px;" class="btnsign">Close</a></div><div id="errDivFSI" class="error caerr" style="display:none;"></div><div class="clear">&nbsp;</div></div></div></div></div></body></html>
			
	<?	?>
<?php else:?>
	<?php echo $data;?>
<?php endif;?>