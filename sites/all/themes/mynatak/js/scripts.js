$('document').ready(function(){
	$('.views-field-field-show-input').hover(function() {
		$('.views-field-field-show-input-2').show();
	});
	
	var $window = $(window),
       $stickyEl = $('#header'),
       elTop = $stickyEl.offset().top;

   $window.scroll(function() {
        $stickyEl.toggleClass('newsticky', $window.scrollTop() > elTop);
    });

/*var add_class_id;
var new_id_class;

	$('.nav_menu > ul >li').mouseenter(function(){
		$('.nav_menu > ul >li').removeClass('open');
		$(this).addClass('open');
		$('#menu_'+prev).removeClass('open');
		add_class_id = $(this).attr('id');
		new_id_class = add_class_id.split('_');
		prev = new_id_class[1];
		$('#menu_'+new_id_class[1]).addClass('open');
	});
	$('.nav_menu > ul >li').mouseleave(function(){
		$('.nav_menu > ul >li').removeClass('open');
		$('#menu_'+prev).removeClass('open');
	});*/
	if($('div').hasClass('image-cycle')) {
		$('.image-cycle').cycle({
				/*height: 260,
				width: 650*/
		});
	}
	var prev;
	
	$('.accordion-link').click(function(){
		var href = $(this).attr('href');
		if (prev == href) {
			$(href).slideUp();
			prev='';
		} else {
			$(prev).slideUp();
			$(href).slideDown();
			prev = href;
		}
		return false;
	});
});