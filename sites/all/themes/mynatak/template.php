<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   mynatak_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: mynatak_field()
 *
 *   where mynatak is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */

function mynatak_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  mynatak_preprocess_html($variables, $hook);
  mynatak_preprocess_page($variables, $hook);
}


/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */

function mynatak_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}


/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function mynatak_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
	 $variables['header_logo_menu'] = _header_menu();
	 $variables['homepage_slider'] = _home_page_slider();
}


/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */

function mynatak_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // mynatak_preprocess_node_page() or mynatak_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
  
  // if ($variables['type'] == 'show_details') {
	// $node = $variables['elements']['#node'];
	// $node_data = show_get_all_details($node);
	// $variables['node'] = $node_data;
  // }
}


/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */

function mynatak_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}


/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */

function mynatak_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}


/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */

function mynatak_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}

function _header_menu() {
 
 global $base_url;
 global $theme;
 global $user;
 
 if ($user->uid == 0) {
 	$text = '<li class="utility_customer utility_link">'.l('Sign In','user').'</li>
		<li class="utility_link">'.l('Register','user/register').'</li>';
 } else {
 	$text = '<li class="utility_link">'.l('Logout','user/logout').'</li>';
 }
 
 $query = db_select('menu_links', 'ml')
												 ->fields('ml', array('link_path','link_title'))
												 ->condition('ml.menu_name','main-menu')
												 ->condition('ml.hidden', 0)
												 ->orderBy('ml.weight', 'ASC')
												 ->execute();
	$header_menus = $query->fetchAll();
	
 $header_output = '<div id="header">
										 <a id="logo" href = "'.url($base_url).'" rel="home">
												<img src="'.$base_url . '/' . drupal_get_path('theme', $theme) . '/images/logo.png" alt="My natak">
										 </a><div id="utilities">
							<ul class="utility_list">'.$text.'
							</ul>
						</div>';
 
																				 
 $header_output .= '<div id="nav" class="full">
											<div class="nav_control"><a href="'.$base_url.'">Navigation</a></div>
											 <div class="nav_menu">
															 <ul class="main_menu">';
 foreach ($header_menus as $header_menu) {
				 $header_output .= '<li>'.l("<strong>".$header_menu->link_title."</strong>", $header_menu->link_path,array('html'=>true,'attributes' => array('class' => array('tab')))).'</li>';
 }
				 $header_output .=' </ul>
														<div class="google_search">
														<form method="get" action="http://www.google.com/search" target="_blank"> 
															<input type="text" name="q" size="25" maxlength="255" value="" /> 
															<input type="submit" value="Google Search" />
														</form>
														</div>
												 </div>
											</div>
										</div>';
 return $header_output;
}

function _home_page_slider() {
	$slider_query = db_select('node', 'n')
					->fields('n', array('nid'))
					->condition('n.status', 1)
					->condition('n.type', 'home_banners')
					->orderBy('n.created','desc')
					->execute()
					->fetchAllKeyed(0,0);
					
	$banner_nodes = node_load_multiple($slider_query);
	
	$quick_search_block = $block = module_invoke('search_block', 'block_view', 'search_block');
	
	$banner_output = '<div id="content-container" style="height:260px">';
	$banner_output .= '<div class="quick-guide">
					<p>Quick Book</p>';
	$banner_output .= render($quick_search_block['content']);
	$banner_output	.=	'</div>
		<div class="image-cycle">';
			
			foreach ($banner_nodes as $nodes) {
				if (!empty($nodes->field_banner_link)) {
						$url = $nodes->field_banner_link['und'][0]['url'];
				}
				if (!empty($nodes->field_shows_banner)) {
						$show_nid[$node['nid']] = $node['nid'];
						$show_node = node_load($nodes->field_shows_banner['und'][0]['nid']);
						$banner_output .= l(theme('image_style', array('style_name' => 'banner_image', 'path' => $show_node->field_show_image['und'][0]['uri'])),$url, array('html' => TRUE));
				} if (!empty($nodes->field_other_banners)) {
						$banner_output .= l(theme('image_style', array('style_name' => 'banner_image', 'path' => $nodes->field_other_banners['und'][0]['uri'])),$url, array('html' => TRUE));
				}
			}
		$banner_output .= '</div>
				</div>';
					   
	return $banner_output;
}

function _get_state_city($nid = '') {
	$title = db_query("SELECT title from node where nid = ".$nid)->fetchField();
	return $title;
	
}