<?php //dpr($node);?>
<div class="fields">
	<h1><?php print $node->title;?></h1>
</div>
<div class="fields">
	<div class="region box">
		<?php if (isset($node->field_actual_photo['und'][0]['uri'])): ?>
			<div class="image-left"><img class="left" src="<?php print file_create_url($node->field_actual_photo['und'][0]['uri']);?>" /></div>
		<?php endif; ?>
			<div class="info">
				<ul>
					<li class="odd">State: </li>
					<li class="odd"><?php if (!empty($node->field_state)) { print _get_state_city($node->field_state['und'][0]['value']); } ?></li>
					<li class="odd">City: </li>
					<li class="odd"><?php if (!empty($node->field_city)) { print _get_state_city($node->field_city['und'][0]['value']); } ?></li>
					
							</ul>
				<?php print $node->body['und'][0]['value'];?>
				<div class="more-link" style="text-align:left;"><a href="<?php print url('search_result',array('query' => array('theater' => $node->nid)))?>">Book Now &raquo;</a></div>
			</div>
	</div>
</div>